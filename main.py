import pyautogui
import time
import pandas
from datetime import datetime
import os
import sys


def zoomrecording(zoomid, zoompwd):
    os.popen("zoom")
    print(datetime.now().strftime('%H:%M'), 'Zoom started')
    time.sleep(4)

    # Click the Plus button
    plus_btn = pyautogui.locateCenterOnScreen('plus_btn.png')
    pyautogui.moveTo(plus_btn)
    pyautogui.click()
    print(datetime.now().strftime('%H:%M'), 'Plus clicked')

    # Type the meeting ID
    time.sleep(1)
    pyautogui.press('Tab', presses=2)
    time.sleep(0.5)
    pyautogui.write(zoomid)
    print(datetime.now().strftime('%H:%M'), 'Zoom ID insert')

    # Disable camera
    time.sleep(0.5)
    pyautogui.press('Tab', presses=4)
    time.sleep(0.5)
    pyautogui.press('Space')
    print(datetime.now().strftime('%H:%M'), 'Camera disabled')
    time.sleep(0.5)
    pyautogui.press('Tab')
    time.sleep(0.5)
    pyautogui.press('Space')
    print(datetime.now().strftime('%H:%M'), 'Join pressed')
    time.sleep(4)

    # Types the password and hits enter
    pyautogui.write(zoompwd)
    print(datetime.now().strftime('%H:%M'), 'Password insert')
    time.sleep(0.5)
    pyautogui.press('enter')
    print(datetime.now().strftime('%H:%M'), 'Enter to conference')

    # Start recording
    time.sleep(5)
    os.popen("obs")
    time.sleep(5)
    print(datetime.now().strftime('%H:%M'), 'Run OBS')
    with pyautogui.hold('Ctrl'):
        with pyautogui.hold('Alt'):
            with pyautogui.hold('Shift'):
                pyautogui.press('R')
    print(datetime.now().strftime('%H:%M'), 'Start recording')
    time.sleep(1)

    with pyautogui.hold('Alt'):
        pyautogui.press('Tab')

    while datetime.now().strftime('%H:%M') != str(row.iloc[0, 1]):
        time.sleep(1)

    with pyautogui.hold('Alt'):
        pyautogui.press('Tab')

    # Stop recording
    time.sleep(1)
    with pyautogui.hold('Ctrl'):
        with pyautogui.hold('Alt'):
            with pyautogui.hold('Shift'):
                pyautogui.press('S')
    print(datetime.now().strftime('%H:%M'), 'Stop recording')
    sys.exit()


# Reading the file
df = pandas.read_csv('schedule.csv')
df['datetime'] = df['date & time'].apply(lambda x: datetime.strptime(x, '%Y.%m.%d %H:%M'))

while True:
    now = datetime.now().replace(microsecond=0)
    if now == df.datetime.min().replace(microsecond=0):
        row = df.loc[df['datetime'] == now]
        zoomrecording(str(row.iloc[0, 2]), str(row.iloc[0, 3]))
        time.sleep(2)
        print(datetime.now().strftime('%H:%M'), 'Procedure initiated')
    else:
        time.sleep(1)
        df = df.loc[df['datetime'] > now]
        timer = df.datetime.min().replace(microsecond=0) - now
        print('Next recording starts in', timer, end='\r')

